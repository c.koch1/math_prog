#!/bin/bash

set -e

cd "$(git rev-parse --show-toplevel)"

#git rm -qf exercises/*.ipynb
for nb in exercises/solutions/*solsource.ipynb; do
    target="exercises/$(basename "$nb" | sed 's/_solsource/_exercise/')"
    filter=$(git check-attr filter "$target" | sed 's/.* //')
    if [[ -n "$filter" ]]; then
        cat "$nb" | $(git config filter."$filter".clean) > "$target"
    else
        cp "$nb" "$target"
    fi
done
